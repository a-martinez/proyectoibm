# Created by Andres Martinez on 4/27/19.
# Copyright © 2019 Quarknet. All rights reserved.



from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.query import Query
from cloudant.document import Document
from tkinter import *
from tkinter import messagebox





#----Conectarse-----
def conectarse():
    client = Cloudant.iam("767bccb8-6a68-4aa4-a6dd-d4e625faa506-bluemix", "CoBN284_HXmjnuhTqtQiqRzgGfK2aAHa_TiTmACkugJU")
    client.connect()


#----Acceder a base de datos-----
def acceder(nombre="shm-quarknet"):
    try:
        BDD = client[nombre]
    except:
        BDD = client.create_database(nombre)
        if BDD.exists():
            print ("'{0}' se creo correctamente.\n".format(nombre))



#----Agregar usuario-----
def agregarUsusario(datos):
    try:
        BDD[str(datos[0])]
    except:
        with Document(BDD, str(datos[0])) as new:
            new["Cedula"]= datos[0]
            new["Nombre"]= datos[1]
            new["Edad"]= datos[2]
            new["Genero"]= datos[3]
        if new.exists():
            return 1
    return 0


#----Acceder a usuario----
def accederUsuario(cedula):
    n={}
    n["Cedula"]=locals()["cedula"]
    query = Query(BDD,selector=n)
    for doc in query.result:
        return doc
    return None

def accederUsuarioU(cedula,cal):
    n={}
    n["Cedula"]=locals()["cedula"]
    query = Query(BDD,selector=n)
    for doc in query.result:
        if doc["Cal"]==cal:
            return doc
        else:
            return 0
    return None

#----Actualizar usuario----
def actualizarUsuario(datos):
    with Document(BDD, str(datos[0])) as new:
        new["Cedula"]= datos[0]
        new["Cal"]= datos[0]
        new["Nombre"]= datos[1]
        new["Edad"]= datos[2]
        new["Genero"]= datos[3]
    if new.exists():
        return 1
    return 0


#----Interfaz Grafica---------------

def Medico():
    top = Tk()
    top.geometry("300x200")
    def acceder():
        dat = ([int(E1.get()),E2.get(),int(E3.get()),E4.get()])
        print (dat)
        if agregarUsusario(dat)==1:
            msg = messagebox.showinfo( "Se agrego!", "El usuario se agrego correctamente")
        else:
            msg = messagebox.showinfo( "No se agrego!", "El usuario  no se agrego correctamente")

    def consultar():
        c = int(E1.get())
        dat = accederUsuario(c)
        if dat is None:
            msg = messagebox.showinfo( "No Existe!", "El usuario buscado no existe")
        else:
            E2.delete(0,END)
            E2.insert(0,dat["Nombre"])
            E3.delete(0,END)
            E3.insert(0,dat["Edad"])
            E4.delete(0,END)
            E4.insert(0,dat["Genero"])

    L0 = Label(top, text = "Agregar usuario")
    L1 = Label(top, text = "Cedula")
    L2 = Label(top, text = "Nombre")
    L3 = Label(top, text = "Edad")
    L4 = Label(top, text = "Genero")

    E1 = Entry(top, bd = 3)
    E2 = Entry(top, bd = 3)
    E3 = Entry(top, bd = 3)
    E4 = Entry(top, bd = 3)

    L0.grid(column=1, row=0)
    L1.grid(column=0, row=1)
    E1.grid(column=1, row=1)
    L2.grid(column=0, row=2)
    E2.grid(column=1, row=2)
    L3.grid(column=0, row=3)
    E3.grid(column=1, row=3)
    L4.grid(column=0, row=4)
    E4.grid(column=1, row=4)

    agregar = Button(top, text="Agregar", command = acceder)
    agregar.grid(column=1, row=5)
    
    consultar = Button(top, text="Consultar", command = consultar)
    consultar.grid(column=1, row=6)
    top.mainloop()


def Usuario():
    top = Tk()
    top.geometry("300x200")

    def consultar():
        c = int(E1.get())
        dat = accederUsuario(c)
        if dat is None:
            msg = messagebox.showinfo( "No Existe!", "El usuario buscado no existe")
        else:
            E2.delete(0,END)
            E2.insert(0,dat["Nombre"])
            E3.delete(0,END)
            E3.insert(0,dat["Edad"])
            E4.delete(0,END)
            E4.insert(0,dat["Genero"])

    L0 = Label(top, text = "Agregar usuario")
    L1 = Label(top, text = "Cedula")
    L2 = Label(top, text = "Nombre")
    L3 = Label(top, text = "Edad")
    L4 = Label(top, text = "Genero")

    E1 = Entry(top, bd = 3)
    E2 = Entry(top, bd = 3)
    E3 = Entry(top, bd = 3)
    E4 = Entry(top, bd = 3)

    L0.grid(column=1, row=0)
    L1.grid(column=0, row=1)
    E1.grid(column=1, row=1)
    L2.grid(column=0, row=2)
    E2.grid(column=1, row=2)
    L3.grid(column=0, row=3)
    E3.grid(column=1, row=3)
    L4.grid(column=0, row=4)
    E4.grid(column=1, row=4)

    consultar = Button(top, text="Consultar", command = consultar)
    consultar.grid(column=1, row=6)
    top.mainloop()
  


#----Desconectarse-----
def desconectarse():
    client.disconnect()







