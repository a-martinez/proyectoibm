//
//  ViewController.swift
//  HMQuarknet
//
//  Created by Andres Martinez on 4/27/19.
//  Copyright © 2019 Quarknet. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITextFieldDelegate {
    //MARK: Properties
    @IBOutlet weak var Cedula: UITextField!
    
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Cedula.delegate = self
    }
    //MARK: Actions

    @IBAction func Agregar(_ sender: UIButton) {
        Cedula.text = "Default Text"
    }
}

