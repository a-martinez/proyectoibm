from django import forms
class FormularioUsuario(forms.Form):
    ID = forms.IntegerField(label='Cédula',max_value=1000000000000)
    Nombre = forms.CharField(max_length=100)
    Apellido = forms.CharField(max_length=100)
    LugarNacimiento = forms.CharField(max_length=100)
    FechaDeNacimiento = forms.DateField(label='Fecha de Nacimiento')
    Sexo = forms.ChoiceField(required=False, widget=forms.Select,choices=(('M','Masculino'),('F','Femenino')))
    RH = forms.ChoiceField(required=False, widget=forms.Select,choices=(('1','+'),('0','-')))
    Tipo = forms.ChoiceField(required=False, widget=forms.Select,choices=(('A','A'),('B','B'),('AB','AB'),('O','O')))
    Eps = forms.ChoiceField(required=False, widget=forms.Select,choices=(('Sanitas','Sanitas'),('Compensar','Compensar'),('Coomeva','Coomeva'),('Sura','Sura')))
class Buscar_Usuario(forms.Form):
    ID = forms.IntegerField(label='Cédula',max_value=1000000000000)
class FormularioEnfermedad(forms.Form):
    ID =forms.IntegerField(label='Cédula',max_value=1000000000000)
    Enfermedad=forms.CharField(label='Enfermedad',max_length=100)
    FechaDeEnfermedad=forms.DateField(label='Fecha de Enfermedad')
class FormularioAlergia(forms.Form):
    ID =forms.IntegerField(label='Cédula',max_value=1000000000000)
    Alergia=forms.CharField(label='Alergia',max_length=100)
class FormularioVacuna(forms.Form):
    ID =forms.IntegerField(label='Cédula',max_value=1000000000000)
    Vacuna=forms.CharField(label='Vacuna',max_length=100)
class FormularioAnalisis(forms.Form):
    Tipo =forms.ChoiceField(required=False, widget=forms.Select,choices=(('E',' Enfermedades'),('V','Vacunas'),('A','Alergias'),('M','Medicamentos'),('N','Natalidad')))
    Analisis=forms.CharField(label='Análisis',max_length=100)
