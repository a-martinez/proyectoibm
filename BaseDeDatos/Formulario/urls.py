from django.urls import path

from . import views

urlpatterns = [
               path('Menu', views.index, name='Menu'),
               path('Menu_Add', views.add, name='Menu_add'),
               path('crear', views.form_view, name='Formulario'),
               path('Mostrar', views.Datos_show, name='Datos'),
               path('DatosMedicos', views.Datos_Medicos_show, name='DatosMedicos'),
               path('Buscar', views.BuscarUsuario, name='Buscar'),
               path('Buscar2', views.Buscar2Usuario, name='Buscar2'),
               path('Enfermedad', views.Agregar_Enfermedad, name='Enfermedad'),
               path('Vacuna', views.Agregar_Vacuna, name='Vacuna'),
               path('Alergia', views.Agregar_Alergia, name='Alergia'),
               path('Analisis', views.Analisis, name='Analisis'),
               path('Error', views.Error, name='Error'),
               path('Exito', views.Exito, name='Exito'),
               path('Upload', views.simple_upload, name='Up'),
               path('Graph', views.Graph, name='Graph'),
               path('Natalidad', views.Natalidad, name='Nat')
               ]
