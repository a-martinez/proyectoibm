# Created by Nicolas Vergara - Andres Martinez on 4/27/19.
# Copyright © 2019 Quarknet. All rights reserved.

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from Formulario.forms import FormularioUsuario, Buscar_Usuario,FormularioVacuna,FormularioAlergia,FormularioEnfermedad,FormularioAnalisis
from Formulario.models import Paciente
from datetime import datetime
import sys
sys.path.append('/Desktop/proyectoibm/BaseDeDatos')
from BDD import conectarse, acceder, desconectarse, agregarUsusario, accederUsuario, actualizarUsuario, actualizarUsuarioL, buscar
from Clasificador import Interfaz
IDENTIFICACION=0

def index(request):
    return render(request, 'Menu.html')
def Natalidad(request):
    return render(request, 'Natalidad.html')
def Graph(request):
    return render(request, 'Graph.html')
def add(request):
    return render(request, 'Add.html')
def Error(request):
    return render(request, 'Error.html')
def Exito(request):
    return render(request, 'Exito.html')

def form_view(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FormularioUsuario(request.POST)
        # check whether it's valid:
        if form.is_valid():
            ID = form.cleaned_data['ID']
            Nombre = form.cleaned_data['Nombre']
            Apellido =form.cleaned_data['Apellido']
            Nacimiento = form.cleaned_data['LugarNacimiento']
            FechaDeNacimiento = form.cleaned_data['FechaDeNacimiento']
            Sexo = form.cleaned_data['Sexo'][0]
            RH = form.cleaned_data['RH'][0]
            Tipo = form.cleaned_data['Tipo'][0]
            Eps = form.cleaned_data['Eps']
            LugarNacimiento = form.cleaned_data['LugarNacimiento']
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            recipients = ['info@example.com']
            
            
            #Base de datos
            cliente = conectarse()
            base = acceder(cliente)
            exito= agregarUsusario(base,[ID,Nombre, Apellido, FechaDeNacimiento.strftime("%d-%b-%Y"),Sexo, LugarNacimiento,Eps,Tipo,RH])
            desconectarse(cliente)
            
            if(exito == True):
                return HttpResponseRedirect('Exito')
            else:
                return HttpResponseRedirect('Error')

        # if a GET (or any other method) we'll create a blank form
    else:
            form = FormularioUsuario()
            Dat=Interfaz()
            Str='Nombre:'+Dat[-1]+' Palabras Sugeridas:'
            for i in range(len(Dat)-1):
                Str=Str +Dat[i]+', '
            return render(request, 'Form.html', {'form': form,'Data':Str})

def Datos_show(request):
    cliente = conectarse()
    base = acceder(cliente)
    Usuario=accederUsuario(base,IDENTIFICACION)
    print(Usuario)
    Rh=None
    dic=None
    if(Usuario==None):
        dic={'pacient': None}
    else:
        if(Usuario['Rh']=='1'):
            Rh="+"
        elif(Usuario['Rh']=='0'):
            Rh="-"
        
        class Pacient:
            ID = Usuario['ID']
            Nombre = Usuario['Nombre']
            Apellido =Usuario['Apellidos']
            FechaDeNacimiento =Usuario['FNacimiento']
            Sexo = Usuario['Genero'][0]
            RH = Rh
            Tipo = Usuario['TSangre']
            Eps = Usuario['Eps']
            LugarNacimiento = Usuario['LNacimiento']
        desconectarse(cliente)
        dic={'paciente': Pacient}
    return render(request, 'Data.html', dic)

def Datos_Medicos_show(request):
    cliente = conectarse()
    base = acceder(cliente)
    Usuario=accederUsuario(base,IDENTIFICACION)
    Rh=None
    dic=None
    if(Usuario==None):
        dic={'pacient': None}
    else:
        if(Usuario['Rh']=='1'):
            Rh="+"
        elif(Usuario['Rh']=='0'):
            Rh="-"
        
        class Pacient:
            Alergias = Usuario['Alergias']
            Nombre = Usuario['Nombre']
            Apellido =Usuario['Apellidos']
            Enfermedades=list()
            for i in range(len(Usuario['Enfermedades'])):
                Enfermedades.append((Usuario['Enfermedades'][i],Usuario['EnfermedadesF'][i]))
            RH = Rh
            Vacunas = Usuario['Vacunas']
        desconectarse(cliente)
        dic={'paciente': Pacient}
    return render(request, 'Data_Medica.html', dic)

def BuscarUsuario(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = Buscar_Usuario(request.POST)
        if form.is_valid():
            ID = form.cleaned_data['ID']
            global IDENTIFICACION
            IDENTIFICACION = ID
            return HttpResponseRedirect('Mostrar')
    else:
            form = Buscar_Usuario()

    return render(request, 'Buscar.html', {'form': form})

def Buscar2Usuario(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = Buscar_Usuario(request.POST)
        if form.is_valid():
            ID = form.cleaned_data['ID']
            global IDENTIFICACION
            IDENTIFICACION = ID
            return HttpResponseRedirect('DatosMedicos')
    else:
        form = Buscar_Usuario()

    return render(request, 'Buscar.html', {'form': form})

def Agregar_Enfermedad(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FormularioEnfermedad(request.POST)
        # check whether it's valid:
        if form.is_valid():
            ID = form.cleaned_data['ID']
            Enfermedad = form.cleaned_data['Enfermedad']
            Fecha_Enfermedad =form.cleaned_data['FechaDeEnfermedad']
            #Base de datos
            cliente = conectarse()
            base = acceder(cliente)
            exito= actualizarUsuarioL(base,ID,'Enfermedades',Enfermedad)
            exito2= actualizarUsuarioL(base,ID,'EnfermedadesF',Fecha_Enfermedad)
            desconectarse(cliente)
            
            if(exito and exito2):
                return HttpResponseRedirect('Exito')
            else:
                return HttpResponseRedirect('Error')

# if a GET (or any other method) we'll create a blank form
    else:
        form = FormularioEnfermedad()
        Dat=Interfaz()
        Str='Nombre:'+Dat[-1]+' Palabras Sugeridas:'
        for i in range(len(Dat)-1):
            Str=Str +Dat[i]+', '
        return render(request, 'Enfermedad.html', {'form': form,'Data':Str})

def Agregar_Vacuna(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FormularioVacuna(request.POST)
        # check whether it's valid:
        if form.is_valid():
            ID = form.cleaned_data['ID']
            Vacuna = form.cleaned_data['Vacuna']
            #Base de datos
            cliente = conectarse()
            base = acceder(cliente)
            exito= actualizarUsuarioL(base,ID,'Vacunas',Vacuna)
            desconectarse(cliente)
            
            if(exito):
                return HttpResponseRedirect('Exito')
            else:
                return HttpResponseRedirect('Error')

# if a GET (or any other method) we'll create a blank form
    else:
        form = FormularioVacuna()
        Dat=Interfaz()
        Str='Nombre:'+Dat[-1]+' Palabras Sugeridas:'
        for i in range(len(Dat)-1):
            Str=Str +Dat[i]+', '
        return render(request, 'Enfermedad.html', {'form': form,'Data':Str})
def Agregar_Alergia(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FormularioAlergia(request.POST)
        # check whether it's valid:
        if form.is_valid():
            ID = form.cleaned_data['ID']
            Alergia = form.cleaned_data['Alergia']
            #Base de datos
            cliente = conectarse()
            base = acceder(cliente)
            exito= actualizarUsuarioL(base,ID,'Alergias',Alergia)
            desconectarse(cliente)
            
            if(exito):
                return HttpResponseRedirect('Exito')
            else:
                return HttpResponseRedirect('Error')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = FormularioAlergia()
        Dat=Interfaz()
        Str='Nombre:'+Dat[-1]+' Palabras Sugeridas:'
        for i in range(len(Dat)-1):
            Str=Str +Dat[i]+', '
        return render(request, 'Enfermedad.html', {'form': form,'Data':Str})
def Analisis(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FormularioAnalisis(request.POST)
        # check whether it's valid:
        if form.is_valid():
            Tipo = form.cleaned_data['Tipo']
            Analisis = form.cleaned_data['Analisis']
            #Base de datos
            cliente = conectarse()
            base = acceder(cliente)
            exito= 0
            desconectarse(cliente)
            print(Tipo)
            if(Tipo=='N'):
                return HttpResponseRedirect('Natalidad')
            else:
                return HttpResponseRedirect('Graph')
    else:
        form = FormularioAnalisis()
        Dat=Interfaz()
        Str='Nombre:'+Dat[-1]+' Palabras Sugeridas:'
        for i in range(len(Dat)-1):
            Str=Str +Dat[i]+', '
        return render(request, 'Enfermedad.html', {'form': form,'Data':Str})
def simple_upload(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        print('la dirección es', uploaded_file_url)
        return render(request, 'table.html', {
                      'uploaded_file_url': uploaded_file_url
                      })
    return render(request, 'TableDoc.html')
