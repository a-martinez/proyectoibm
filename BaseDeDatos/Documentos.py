import json
from os.path import abspath
from ibm_watson import CompareComplyV1
import numpy as np



compare_comply = CompareComplyV1(
                                 version='2018-10-15',
                                 iam_apikey='1wwwApCKWnnxRwOGdT7ctT25wHwF-bLEcMxZzjaA61U2',
                                 url='https://gateway.watsonplatform.net/compare-comply/api'
                                 )
contract = abspath('6359521438709849575156592.pdf')
with open(contract, 'rb') as file:
    result = compare_comply.convert_to_html(file).get_result()
print(json.dumps(result, indent=2))

'''
JFile=json.dumps(result, indent=2)
f = open("Table.html", "a")
f.write(JFile)
f.close()
#print(JFile)
'''
