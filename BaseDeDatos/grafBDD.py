from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.query import Query
from cloudant.document import Document
import numpy as np
import time
import matplotlib.pyplot as plt


client = Cloudant.iam("767bccb8-6a68-4aa4-a6dd-d4e625faa506-bluemix", "CoBN284_HXmjnuhTqtQiqRzgGfK2aAHa_TiTmACkugJU")
client.connect()
BDD=None
try:
    BDD = client["shm-quarknet"]
except:
    BDD = client.create_database("shm-quarknet")
    if BDD.exists():
        print ("'{0}' se creo correctamente.\n".format("shm-quarknet"))


def grafica(BDD,enfermedad):
    N=np.zeros(101)
    Con=np.zeros(101)
    Y=np.linspace(1920,2020,101)
    C=np.zeros(101)
    L=[]
    D=[]

    ConO=np.zeros(101)
    CO=np.zeros(101)
    LO=[]
    DO=[]

    q=0
    for i in BDD:
        edad = 2019-int(((i["FNacimiento"]).split('-'))[2])
        if edad > 101:
            continue
        N[edad]=N[edad]+1
        if enfermedad in i["Enfermedades"]:
            Con[edad]=Con[edad]+1
            y=int((((i["EnfermedadesF"])[(i["Enfermedades"]).index(enfermedad)]).split('-'))[2])
            C[y-1950]=C[y-1950]+1
            if i["LNacimiento"] in L:
                D[L.index(i["LNacimiento"])]=D[L.index(i["LNacimiento"])]+1
            else:
                L.append(i["LNacimiento"])
                D.append(1)
        time.sleep(.002)
        print(q)
        q=q+1



    G=[]
    for j in L:
        G.append((j.split(","))[0])
    GO=[]
    for j in LO:
        GO.append((j.split(","))[0])

    plt.scatter(Y,C,c="lightsalmon")
    plt.ylabel("Contagios de "+enfermedad)
    plt.xlabel("Año")
    plt.savefig("Contagios.pdf",dpi=100)
    plt.close()
    plt.scatter(Y,N,c="lightsalmon")
    plt.ylabel("Nacimientos")
    plt.xlabel("Año de nacimiento")
    plt.savefig("Natalidad.pdf",dpi=100)
    plt.close()
    plt.scatter(Y,Con,c="lightsalmon")
    plt.ylabel("Primera fecha"+ enfermedad)
    plt.xlabel("Año de nacimiento")
    plt.savefig("NatalidadContagiados.pdf",dpi=100)
    plt.close()
    fig = plt.figure(figsize=(15,20))
    plt.scatter(L,D,c="lightsalmon")
    plt.ylabel("Primera fecha"+ enfermedad)
    plt.xlabel("Municipio")
    plt.xticks(L, G, rotation='vertical')
    plt.margins(0.1)
    plt.savefig("LugarContagiados.pdf",dpi=100)
    plt.subplots_adjust(bottom=0.25)
    plt.close()



grafica(BDD,"Amigdalitis")



client.disconnect()
