#!/usr/bin/env python3

# Created by Andres Martinez on 4/27/19.
# Copyright © 2019 Quarknet. All rights reserved.


from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.query import Query
from cloudant.document import Document
from tkinter import *
from tkinter import messagebox


#----Conectarse-----
def conectarse(apiKey="CoBN284_HXmjnuhTqtQiqRzgGfK2aAHa_TiTmACkugJU", user="767bccb8-6a68-4aa4-a6dd-d4e625faa506-bluemix"):
    client = Cloudant.iam(user, apiKey)
    client.connect()
    return client

#----Acceder a base de datos-----
def acceder(client,nombre="shm-quarknet"):
    BDD=None
    try:
        BDD = client[nombre]
    except:
        BDD = client.create_database(nombre)
        if BDD.exists():
            print ("'{0}' se creo correctamente.\n".format(nombre))
    return BDD


#----Agregar usuario-----
def agregarUsusario(BDD,datos):
    try:
        BDD[str(datos[0])]
    except:
        with Document(BDD, str(datos[0])) as new:
            new["ID"]= datos[0]
            new["Nombre"]= datos[1]
            new["Apellidos"]= datos[2]
            new["FNacimiento"]= datos[3]
            new["Genero"]= datos[4]
            new["LNacimiento"]= datos[5]
            new["Eps"]= datos[6]
            new["TSangre"]= datos[7]
            new["Rh"]= datos[8]
            new["Alergias"]=[]
            new["Enfermedades"]=[]
            new["EnfermedadesF"]=[]
            new["Vacunas"]=[]
            new["Medicamentos"]=[]
            new["MedicamentosF"]=[]
            new["MedicamentosD"]=[]
        if new.exists():
            return 1
    return 0

#----Acceder a usuario----
def accederUsuario(BDD,id):
    id=str(id)
    try:
        return BDD[locals()["id"]]
    except:
            return None

def accederUsuarioU(BDD,id):
    n={}
    n["ID"]=locals()["id"]
    query = Query(BDD,selector=n)
    for doc in query.result:
        return doc
    return None

#----Actualizar usuario----
def actualizarUsuario(BDD,datos):
    with Document(BDD, str(datos[0])) as new:
        new["ID"]= datos[0]
        new["Nombre"]= datos[1]
        new["Apellidos"]= datos[2]
        new["FNacimiento"]= datos[3]
        new["Genero"]= datos[4]
        new["LNacimiento"]= datos[5]
        new["Eps"]= datos[6]
        new["TSangre"]= datos[7]
        new["Rh"]= datos[8]
        new["Alergias"]=[9]
        new["Enfermedades"]=[10]
        new["EnfermedadesF"]=[11]
        new["Vacunas"]=[12]
        new["Medicamentos"]=[13]
        new["MedicamentosF"]=[14]
        new["MedicamentosD"]=[15]
    return 1


#----Actualizar usuario por campo----
def actualizarUsuario(BBD, id, nombre, valor):
    id=str(id)
    if id in BBD:
        m = BBD[id]
        m[nombre]=valor
        m.save()
        return 1
    return 0

def actualizarUsuarioL(BBD, id, nombre, valor):
    id=str(id)
    if id in BBD:
        m = BBD[id]
        m[nombre].append(valor)
        m.save()
        return 1
    return 0

#----Buscar cosas-----
def buscar(BDD, campo, valor):
    n={}
    n[campo]=locals()["valor"]
    query = Query(BDD,selector=n)
    L=[]
    for doc in query.result:
        L.append(doc)
    return L

#----Desconectarse-----
def desconectarse(client):
    client.disconnect()


