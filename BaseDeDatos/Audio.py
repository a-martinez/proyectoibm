from ibm_watson import SpeechToTextV1
from ibm_watson.websocket import RecognizeCallback, AudioSource
from os.path import join, dirname
import json

speech_to_text = SpeechToTextV1(
                                iam_apikey='x7LUsFnX_SpwelcBB7jElJe9UG_QHd_jMjSt06cMSn5s',
                                url='https://stream.watsonplatform.net/speech-to-text/api'
                                )

with open(join(dirname(__file__), './.', 'AudioCita.mp3'),
          'rb') as audio_file:
    speech_recognition_results = speech_to_text.recognize(audio=audio_file, content_type='audio/mp3', word_alternatives_threshold=0.9,keywords=['Cedula', 'tornado', 'tornadoes'],keywords_threshold=0.5).get_result()
print(json.dumps(speech_recognition_results, indent=2))
