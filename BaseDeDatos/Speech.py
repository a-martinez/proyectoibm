from ibm_watson import SpeechToTextV1
from ibm_watson.websocket import RecognizeCallback, AudioSource
from os.path import join, dirname
import json

speech_to_text = SpeechToTextV1(
                                iam_apikey='x7LUsFnX_SpwelcBB7jElJe9UG_QHd_jMjSt06cMSn5s',
                                url='https://stream.watsonplatform.net/speech-to-text/api'
                                )

class MyRecognizeCallback(RecognizeCallback):
    def __init__(self):
        RecognizeCallback.__init__(self)
    
    def on_data(self, data):
        savetext=json.dumps(data, indent=2)
        f = open("Text.txt", "a")
        f.write(savetext)
        f.close()
        print(json.dumps(data, indent=2))
    
    def on_error(self, error):
        print('Error received: {}'.format(error))
    
    def on_inactivity_timeout(self, error):
        print('Inactivity timeout: {}'.format(error))

myRecognizeCallback = MyRecognizeCallback()

with open(join(dirname(__file__), './.', 'AudioCita.mp3'),
          'rb') as audio_file:
    audio_source = AudioSource(audio_file)
    speech_to_text.recognize_using_websocket(
                                             audio=audio_source,
                                             content_type='audio/mp3',
                                             recognize_callback=myRecognizeCallback,
                                             model='es-ES_BroadbandModel',
                                             keywords=['radiografía', 'cédula'],
                                             keywords_threshold=0.5,
                                             max_alternatives=3)

