import numpy as np
import json
from watson_developer_cloud import NaturalLanguageUnderstandingV1
from watson_developer_cloud.natural_language_understanding_v1 import Features, EntitiesOptions, ConceptsOptions

natural_language_understanding = NaturalLanguageUnderstandingV1(
                                                                version='2018-11-16',
                                                                iam_apikey='UJZdXcda25c6Tv3Cin5mB5btnTNBciQWaheV_mGbo7qW',
                                                                url='https://gateway.watsonplatform.net/natural-language-understanding/api')
f=open('Text.txt','r')
texto=f.read()
#print(texto)
def AnalizadordeTextos(texto):
    response = natural_language_understanding.analyze(text=texto,features=Features(entities=EntitiesOptions(sentiment=False),concepts=ConceptsOptions())).get_result()
    return(response)
                                                            
def AsignarNombrePaciente(DiccEntidades):
    max=0
    a={}
    for i in DiccEntidades:
        if i["type"]=="Person" and (i["relevance"]>max):
            max=i["relevance"]
            a=i
    return(a["text"])

def AsignarConceptos(DiccConceptos):
    Conceptos=[]
    for i in DiccConceptos:
        if i['relevance']>=0.7 :
            Conceptos.append(i["text"])
    return(Conceptos)

def Interfaz():
    Dicc=AnalizadordeTextos(texto)
    return([AsignarNombrePaciente(Dicc['entities']),AsignarConceptos(Dicc['concepts'])])
